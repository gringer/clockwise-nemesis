extends RigidBody2D

@export var tryingToMove = false
@export var targetAngle = -1

var kinetic_energy = 0.0
var score = 0
var energy = 100
var cooldown = 0
var thinkingCooldown = 0
var playerID = 1
var shouldTele = false
var currentCell: Vector2i
var deadEndDist: Dictionary
var visited: Dictionary
var nextCherryCell = Vector2i(0,0)

@onready var rootNode = get_parent()
@onready var tileMap:TileMap = rootNode.get_node("walls_TileMap")

# Called when the node enters the scene tree for the first time.
func _ready():
	tileClear()

func tileClear():
	print("Clearing move stack")
	tileMap.clear_layer(7)
	deadEndDist.clear()
	visited.clear()
	currentCell = Vector2i(0,0)
	pass

func addScore(delta):
	tileClear()
	score += delta

func setTele():
	tileClear()
	shouldTele = true

# Finds the shortest path to the cherry
# Breadth-first search. Don't need to care about loops because it's a perfect maze
func findPath():
	var cherryNode: Area2D = rootNode.get_node("cherry_Area2D")
	var cherryPos: Vector2i = tileMap.local_to_map(tileMap.to_local(cherryNode.global_position))
	if(currentCell == cherryPos):
		print("The cherry is here!")
		return(currentCell)
	print("Cherry Pos: %s" % str(cherryPos))
	var visitedCells: Dictionary
	var cellPathOrigin: Dictionary
	var cellPathDir: Dictionary
	var cellsToCheck = Array()
	var checkedCells = 0
	cellsToCheck.append(currentCell)
	tileMap.clear_layer(7)
	while(cellsToCheck.size() > 0):
		var checkCell = cellsToCheck.pop_front()
		var adj = Vector2i(abs(checkCell[1]) % 2, 0)
		if(!(checkCell in visitedCells)):
			checkedCells += 1
			visitedCells[checkCell] = true
			var borderedCells = 0
			for checkDir in range(0, 6):
				var dirCell = checkCell + adj + rootNode.DIST_DELTAS[checkDir]
				if((checkDir == 0) or (checkDir == 3)):
					dirCell = checkCell + rootNode.DIST_DELTAS[checkDir]
				var dRes: Vector2i = tileMap.get_cell_atlas_coords(0, dirCell)
				var tRes: Vector2i = tileMap.get_cell_atlas_coords(checkDir+1, checkCell)
				if((dRes == rootNode.FLOOR_TILE_ID) && (tRes == rootNode.TILEMAP_MISSING)): # if there is a gap in that direction
					if(!(dirCell in cellPathOrigin)):
						cellPathOrigin[dirCell] = checkCell
						cellPathDir[dirCell] = checkDir
						cellsToCheck.push_back(dirCell)
					borderedCells += 1
			if(borderedCells == 0):
				print("No adjacent cells for %s" % checkCell)
			pass
		if(checkCell == cherryPos):
			cellsToCheck.clear()
			cellsToCheck.append(checkCell)
			break
	if(cellsToCheck.size() > 0):
		print("Found the cherry!")
		var cherryPath = Array()
		var pathVisited: Dictionary
		var nextCell = cherryPos
		var lastCell = cherryPos
		var nextDir = -1
		tileMap.set_cell(7, nextCell, 0, Vector2i(2,0))
		while((nextCell != currentCell) and (nextCell in cellPathOrigin) and (nextCell in cellPathDir)):
			pathVisited[nextCell] = true
			lastCell = nextCell
			nextCell = cellPathOrigin[nextCell]
			nextDir = cellPathDir[nextCell]
			tileMap.set_cell(7, nextCell, 0, Vector2i(2,0))
			cherryPath.append(nextCell)
		if(nextCell == currentCell):
			return(lastCell)
		else:
			return(cherryPos)
	else:
		print("Couldn't find the cherry, but checked %d cells!" % checkedCells)
		return(cherryPos)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	kinetic_energy = linear_velocity.length()
	cooldown = min(100, cooldown + delta * 100.0)
	$CollisionShape2D/Sprite2D.modulate.a = cooldown / 100.0
	## Artificial Intelligence
	##  - On entry to a new cell, stop doing everything for 1.5 seconds
	##  - 1.5s after entering a new cell, choose a random available exit
	##  - Attempt to rotate the world so that the enemy can leave through that exit 
	var newCell: Vector2i = tileMap.local_to_map(tileMap.to_local(global_position))
	if(newCell != currentCell):
		var adj = Vector2i(abs(newCell[1]) % 2, 0)
		for checkDir in range(0, 6):
			var dirCell = newCell + adj + rootNode.DIST_DELTAS[checkDir]
			if((checkDir == 0) or (checkDir == 3)):
				dirCell = newCell + rootNode.DIST_DELTAS[checkDir]
		if(currentCell in deadEndDist):
			if(deadEndDist[currentCell] >= 0):
				print("Dead end distance: %d" % deadEndDist[currentCell])
			pass
		if(!(newCell in visited)):
			visited[newCell] = true
			pass
		print("New Cell - %s! Gotta look around a bit." % str(newCell))
		tryingToMove = false
		currentCell = newCell
		thinkingCooldown = randf_range(0, 1.500)
		nextCherryCell = findPath()
		adj = Vector2i(abs(currentCell[1]) % 2, 0)
		var deadEndDists = Array()
		var maxDeadEndDist = -1
		var openDirCount = 0
		var deadDirCount = 0
		# do dead end checking
		for checkDir in range(0, 6):
			var dirCell = currentCell + adj + rootNode.DIST_DELTAS[checkDir]
			if((checkDir == 0) or (checkDir == 3)):
				dirCell = currentCell + rootNode.DIST_DELTAS[checkDir]
			var dRes: Vector2i = tileMap.get_cell_atlas_coords(0, dirCell)
			var tRes: Vector2i = tileMap.get_cell_atlas_coords(checkDir+1, currentCell)
			if((dRes == rootNode.FLOOR_TILE_ID) && (tRes == rootNode.TILEMAP_MISSING)): # if there is a gap in that direction
				if(dirCell in deadEndDist):
					var checkDist = deadEndDist[dirCell]
					if(checkDist > maxDeadEndDist):
						maxDeadEndDist = checkDist
					openDirCount += 1
					deadDirCount += 1
					deadEndDists.append(checkDist)
				else:
					openDirCount += 1
					deadEndDists.append(-1)
		if(openDirCount == 1):
			deadEndDist[currentCell] = 0
			print("Found a dead end; no need to think about this")
			thinkingCooldown = delta/2
		elif(openDirCount <= (deadDirCount + 1)):
			if(!(currentCell in deadEndDist)):
				deadEndDist[currentCell] = maxDeadEndDist + 1
			print("All exits are dead ends; furthest distance is %d" % (maxDeadEndDist + 1))
	elif(thinkingCooldown > 0):
		thinkingCooldown -= delta
		if(thinkingCooldown <= 0):
			thinkingCooldown = 0
			print("Okay, time to choose an exit...")
			var adj = Vector2i(abs(currentCell[1]) % 2, 0)
			var openDirs = Array()
			var deadEndDists = Array()
			var livingDirs = Array()
			var maxDeadEndDist = -1
			for checkDir in range(0, 6):
				var tRes: Vector2i = tileMap.get_cell_atlas_coords(checkDir+1, currentCell)
				if(tRes == rootNode.TILEMAP_MISSING):
#					print("Exit found at direction %d" % checkDir)
					openDirs.append(checkDir * 2 * PI / 6 + PI)
					var dirCell = currentCell + adj + rootNode.DIST_DELTAS[checkDir]
					if((checkDir == 0) or (checkDir == 3)):
						dirCell = currentCell + rootNode.DIST_DELTAS[checkDir]
					if(dirCell == nextCherryCell):
						print("Trying to move towards the cherry (cell %s)" % nextCherryCell)
						openDirs.clear()
						livingDirs.clear()
						openDirs.append(checkDir * 2 * PI / 6 + PI)
						livingDirs.append(checkDir * 2 * PI / 6 + PI)
						break
					if(dirCell in deadEndDist):
#						print("  ... it's dead, Jim")
						var checkDist = deadEndDist[dirCell]
						if(checkDist > maxDeadEndDist):
							maxDeadEndDist = checkDist
						deadEndDists.append(checkDist)
					else:
						deadEndDists.append(-1)
						livingDirs.append(checkDir * 2 * PI / 6 + PI)
			# If there are open directions to cells with no calculated dead end distance, choose
			#    randomly from those
			if(livingDirs.size() > 0):
				print("Living directions exist: %s" % str(livingDirs))
				openDirs = livingDirs
			else:
				print("No living directions available; choosing randomly from the dead directions")
			# Otherwise, choose randomly from the available exits
			if(openDirs.size() > 0):
				openDirs.shuffle()
				targetAngle = openDirs[0]
				if(rootNode.world_gravDir == -1):
					targetAngle += PI
				while(targetAngle > PI):
					targetAngle = targetAngle - 2*PI
				while(targetAngle < -PI):
					targetAngle = targetAngle + 2*PI
				print("Trying to move to %0.2f" % targetAngle)
				tryingToMove = true

func _integrate_forces(state):
	if(shouldTele):
		shouldTele = false
		var xform = state.get_transform()
		xform.origin = rootNode.getRespawnPoint()
		state.set_transform(xform)
	if((cooldown == 100) and (get_contact_count() > 0)):
		var bodies = get_colliding_bodies()
		for bodyObj in bodies:
			if((bodyObj.name != name) and (bodyObj.name.contains("_ball"))):
				if(kinetic_energy < bodyObj.kinetic_energy):
					cooldown = 0
					bodyObj.addScore(10)
					rootNode.updateText()
					var xform = state.get_transform()
					xform.origin = rootNode.getRespawnPoint()
					state.set_transform(xform)


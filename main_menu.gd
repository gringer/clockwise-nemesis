extends Node2D

@export var world_rotation: float = PI/2 - PI/6
@export var world_gravDir = 1
@export var inSelection = false

var voteStrength1 = 1.0
var voteEnergy1 = 100.0
var teleportGravDir1 = 1
var voteAddSpeed = 50.0
var voteSubSpeed = 20.0
var flipProgress = 0.0
var voteAdjust = 100
var maxEnergy = 120
var minEnergy = 10
var shiftToTarget = false

@onready var arrowNode =  $gravityArrow_Node2D
@onready var arrowDisplayNode = $gravityArrow_Node2D/gravityDir_Sprite2D
@onready var flipProgressNodeR = $gravityArrow_Node2D/ProgressBarR
@onready var flipProgressNodeL = $gravityArrow_Node2D/ProgressBarL
@onready var tileMap = $menu_TileMap
@onready var ball1 = $menu_ball
@onready var optionsArea = $options_Area2D

var newGravity: Vector2
var targetAngle: float = world_rotation

# Tilemap constants
const FLOOR_TILE_ID = Vector2i(0, 0)
const WALL_TILE_ID  = Vector2i(1, 0)
const TILEMAP_MISSING = Vector2i(-1,-1)
const TILE_W = 0
const TILE_NW = 1
const TILE_NE = 2
const TILE_E = 3
const TILE_SE = 4
const TILE_SW = 5
const WALL_PUNCH_PROB = 0.5
const MAX_PATH_LENGTH = 12

func setTargetAngle(tAngle):
	shiftToTarget = true
	targetAngle = tAngle

func setProgress(progressValue):
	flipProgress = progressValue

func setSelectedOption(optionName):
	print("Option selected: %s" % optionName)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var minDelta = voteStrength1 * voteAdjust * delta * 2 * PI / 360.0
	var shiftAngle = targetAngle - world_rotation
	while(abs(shiftAngle) > PI):
		shiftAngle += -sign(shiftAngle) * 2 * PI
	if(shiftToTarget and (abs(shiftAngle) > minDelta)):
		shiftAngle = targetAngle - world_rotation
		while(abs(shiftAngle) > PI):
			shiftAngle += -sign(shiftAngle) * 2 * PI
	elif(shiftToTarget):
		world_rotation = targetAngle
		targetAngle = world_rotation
		shiftToTarget = false
	else:
		shiftAngle = 0
	if (Input.is_action_pressed("player_1_cw") or (shiftToTarget and (shiftAngle < 0))):
		if(Input.is_action_pressed("player_1_cw")):
			shiftToTarget = false
		voteStrength1 = voteEnergy1 / 100.0
		world_rotation -= voteStrength1 * voteAdjust * delta * 2 * PI / 360.0
	elif (Input.is_action_pressed("player_1_ccw") or (shiftToTarget and (shiftAngle > 0))):
		if(Input.is_action_pressed("player_1_ccw")):
			shiftToTarget = false
		voteStrength1 = voteEnergy1 / 100.0
		world_rotation += voteStrength1 * voteAdjust * delta * 2 * PI / 360.0
	elif (inSelection and (Input.is_action_pressed("player_1_gravFlip") or 
			Input.is_mouse_button_pressed(MOUSE_BUTTON_LEFT))):
		shiftToTarget = false
		flipProgress += voteStrength1 * voteAdjust * delta
	PhysicsServer2D.area_set_param(get_world_2d().space,
		PhysicsServer2D.AREA_PARAM_GRAVITY_VECTOR,
		world_gravDir * Vector2(cos(world_rotation), sin(world_rotation)))
	newGravity = PhysicsServer2D.area_get_param(
		get_world_2d().space,
		PhysicsServer2D.AREA_PARAM_GRAVITY_VECTOR)
	flipProgressNodeR.value = flipProgress
	flipProgressNodeL.value = flipProgress
	arrowNode.rotation = atan2(newGravity[1], newGravity[0]) - (PI/2 * world_gravDir)
	arrowDisplayNode.rotation =  (PI/2 * world_gravDir)
	if(flipProgress >= 100):
		optionsArea.select_option()

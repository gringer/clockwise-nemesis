extends TileMap

@export var world_rotation = PI/2

var voteStrength1 = 1.0
var voteEnergy1 = 100.0
var voteStrength2 = 1.0
var voteEnergy2 = 100.0
var voteStrength3 = 1.0
var voteEnergy3 = 100.0
var voteAddSpeed = 50.0
var voteSubSpeed = 20.0
var voteAdjust = 50

var maxEnergy = 120
var minEnergy = 10
var arrowSprite: Sprite2D
var arrowNode: Node2D
var rootNode: Node
var newGravity: Vector2
var spawnPoints: Array

# Called when the node enters the scene tree for the first time.
func _ready():
	rootNode = get_parent()
	arrowNode = get_node("gravityArrow_Node2D")
	spawnPoints = get_used_cells_by_id(0,-1,Vector2i(3,3))
	pass # Replace with function body.

func get_spawn_point():
	var retVal = Vector2(spawnPoints[randi_range(0,spawnPoints.size()-1)])
	return (retVal * 25 + position)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_pressed("player_1_cw"):
		voteStrength1 = voteEnergy1 / 100.0
		world_rotation -= voteStrength1 * voteAdjust * delta * 2 * PI / 360.0
		voteEnergy1 = max(minEnergy, voteEnergy1 - voteSubSpeed * delta)
	elif Input.is_action_pressed("player_1_ccw"):
		voteStrength1 = voteEnergy1 / 100.0
		world_rotation += voteStrength1 * voteAdjust * delta * 2 * PI / 360.0
		voteEnergy1 = max(minEnergy, voteEnergy1 - voteSubSpeed * delta)
	else:
		voteEnergy1 = min(maxEnergy, voteEnergy1 + voteAddSpeed * delta)
	rootNode.get_node("player_1_ball/Label").text = "%0.0f" % voteEnergy1;
	if Input.is_action_pressed("player_2_cw"):
		voteStrength2 = voteEnergy2 / 100.0
		world_rotation -= voteStrength2 * voteAdjust * delta * 2 * PI / 360.0
		voteEnergy2 = max(minEnergy, voteEnergy2 - voteSubSpeed * delta)
	elif Input.is_action_pressed("player_2_ccw"):
		voteStrength2 = voteEnergy2 / 100.0
		world_rotation += voteStrength2 * voteAdjust * delta * 2 * PI / 360.0
		voteEnergy2 = max(minEnergy, voteEnergy2 - voteSubSpeed * delta)
	else:
		voteEnergy2 = min(maxEnergy, voteEnergy2 + voteAddSpeed * delta)
	rootNode.get_node("player_2_ball/Label").text = "%0.0f" % voteEnergy2;
	if Input.is_action_pressed("player_3_cw"):
		voteStrength3 = voteEnergy3 / 100.0
		world_rotation -= voteStrength3 * voteAdjust * delta * 2 * PI / 360.0
		voteEnergy3 = max(minEnergy, voteEnergy3 - voteSubSpeed * delta)
	elif Input.is_action_pressed("player_3_ccw"):
		voteStrength3 = voteEnergy3 / 100.0
		world_rotation += voteStrength3 * voteAdjust * delta * 2 * PI / 360.0
		voteEnergy3 = max(minEnergy, voteEnergy3 - voteSubSpeed * delta)
	else:
		voteEnergy3 = min(maxEnergy, voteEnergy3 + voteAddSpeed * delta)
	rootNode.get_node("player_3_ball/Label").text = "%0.0f" % voteEnergy3;
	if Input.is_action_pressed("ui_cancel"):
		get_tree().quit()
		
	PhysicsServer2D.area_set_param(get_world_2d().space,
		PhysicsServer2D.AREA_PARAM_GRAVITY_VECTOR,
		Vector2(cos(world_rotation), sin(world_rotation)))
	newGravity = PhysicsServer2D.area_get_param(
		get_world_2d().space,
		PhysicsServer2D.AREA_PARAM_GRAVITY_VECTOR)
	arrowNode.rotation = atan2(newGravity[1], newGravity[0]) - PI/2

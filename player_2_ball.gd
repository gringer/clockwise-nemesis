extends RigidBody2D

var startPos: Vector2
var rootNode: Node
var walls: TileMap
var kinetic_energy = 0.0
var score = 0
var energy = 100
var cooldown = 100
var playerID = 2

# Called when the node enters the scene tree for the first time.
func _ready():
	rootNode = get_parent()
	walls = rootNode.get_node("walls_TileMap")
	position = walls.get_spawn_point()
	startPos = position

func _physics_process(delta):
	kinetic_energy = linear_velocity.length()
	cooldown = min(100, cooldown + delta * 100.0)
	$collisionShape2D/Sprite2D.modulate.a = cooldown / 100.0
#	$Label.text = "%0.3f" % kinetic_energy
	$Label.rotation = -rotation

func _integrate_forces(state):
	if((cooldown == 100) and (get_contact_count() > 0)):
		var bodies = get_colliding_bodies()
		for bodyObj in bodies:
			if((bodyObj.name != name) and (bodyObj.name.contains("_ball"))):
				if(kinetic_energy < bodyObj.kinetic_energy):
					cooldown = 0
					bodyObj.score += 10
					rootNode.update_text()
					var xform = state.get_transform()
					xform.origin = walls.get_spawn_point()
					state.set_transform(xform)
			elif(bodyObj.name == "cherries"):
				if(bodyObj.lastPlayerTouch == playerID):
					score += bodyObj.lastPlayerScore
					bodyObj.lastPlayerScore = min(32, bodyObj.lastPlayerScore * 2)
				else:
					score += 1
					bodyObj.lastPlayerTouch = playerID
					bodyObj.lastPlayerScore = 2
				rootNode.update_text()
				bodyObj.respawn()

extends StaticBody2D

@export var initialCherryScore = 1
var lastPlayerTouch = -1
var lastPlayerScore = 1
var walls: TileMap


# Called when the node enters the scene tree for the first time.
func _ready():
	walls = get_parent().get_node("walls_TileMap")
	pass # Replace with function body.
	
func respawn():
	position = walls.get_spawn_point()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

extends Node

var player_1_ball: RigidBody2D
var player_2_ball: RigidBody2D
var player_3_ball: RigidBody2D

var text_score_player1 : Label
var text_score_player2 : Label
var text_score_player3 : Label

# Called when the node enters the scene tree for the first time.
func _ready():
	player_1_ball = get_node("player_1_ball")
	player_2_ball = get_node("player_2_ball")
	player_3_ball = get_node("player_3_ball")
	text_score_player1 = get_node("CanvasLayer/label_points_1")
	text_score_player2 = get_node("CanvasLayer/label_points_2")
	text_score_player3 = get_node("CanvasLayer/label_points_3")
	pass # Replace with function body.

func update_text():
	text_score_player1.text = "Score 1: %d" % player_1_ball.score
	text_score_player2.text = "Score 2: %d" % player_2_ball.score
	text_score_player3.text = "Score 3: %d" % player_3_ball.score

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

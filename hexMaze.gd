extends Node2D

@export var world_rotation = PI/2
@export var world_gravDir = 1

var voteStrength1 = 1.0
var voteEnergy1 = 100.0
var teleportActive1 = false
var teleportGravDir1 = 1
var voteStrength2 = 1.0
var voteEnergy2 = 100.0
var teleportActive2 = false
var teleportGravDir2 = 1
var voteStrength3 = 1.0
var voteEnergy3 = 100.0
var teleportActive3 = false
var teleportGravDir3 = 1
var voteAddSpeed = 50.0
var voteSubSpeed = 20.0
var flipProgress = 0.0
var voteAdjust = 50
var maxEnergy = 120
var minEnergy = 10

@onready var arrowNode =  $gravityArrow_Node2D
@onready var arrowDisplayNode = $gravityArrow_Node2D/gravityDir_Sprite2D
@onready var flipProgressNode = $gravityArrow_Node2D/ProgressBar
@onready var tileMap = $walls_TileMap
@onready var ball1 = $player_1_ball
@onready var ball2 = $player_2_ball
@onready var ball3 = $player_3_ball
@onready var cherry = $cherry_Area2D
@onready var cherrySprite = $cherry_Area2D/CollisionPolygon2D/Sprite2D
@onready var scoreText = $gravityArrow_Node2D/scores_RichTextLabel

var newGravity: Vector2

const tileWidth = 68
const tileMaxX = 9
const tileMinX = -9
const tileMaxY = 9
const tileMinY = -9

# Tilemap constants
const FLOOR_TILE_ID = Vector2i(0, 0)
const WALL_TILE_ID  = Vector2i(1, 0)
const TILEMAP_MISSING = Vector2i(-1,-1)
const TILE_W = 0
const TILE_NW = 1
const TILE_NE = 2
const TILE_E = 3
const TILE_SE = 4
const TILE_SW = 5
const WALL_PUNCH_PROB = 0.5
const MAX_PATH_LENGTH = 12

var originalEmptyTiles: Array
var emptyTiles: Array
var tileGroup: Dictionary

func updateText():
	scoreText.text = str("[font_size=60][b]Scores[/b][/font_size]\n\n",
	"[b]Orange:[/b] ",ball1.score,
	"\n[b]Red:[/b] ",ball2.score,
	"\n[b]Blue:[/b] ",ball3.score)
	pass

func getRespawnPoint():
	emptyTiles.shuffle()
	return(tileMap.to_global(tileMap.map_to_local(emptyTiles[0])))

func respawnGame():
	for layerID in range(1, 6):
		tileMap.clear_layer(layerID)
	for origTile in originalEmptyTiles:
		tileMap.set_cell(0, origTile, 0, FLOOR_TILE_ID)
		# Add back walls to stop out of bounds falling
		var adj = abs(origTile[1]) % 2
		if(tileMap.get_cell_atlas_coords(0, origTile + Vector2i(-1 + adj, -1)) == WALL_TILE_ID):
			tileMap.set_cell(TILE_NW + 1, origTile, 0, Vector2i(TILE_NW,1))
		if(tileMap.get_cell_atlas_coords(0, origTile + Vector2i(0 + adj, -1)) == WALL_TILE_ID):
			tileMap.set_cell(TILE_NE + 1, origTile, 0, Vector2i(TILE_NE,1))
		if(tileMap.get_cell_atlas_coords(0, origTile + Vector2i(-1, 0)) == WALL_TILE_ID):
			tileMap.set_cell(TILE_W + 1, origTile, 0, Vector2i(TILE_W,1))
		if(tileMap.get_cell_atlas_coords(0, origTile + Vector2i(1, 0)) == WALL_TILE_ID):
			tileMap.set_cell(TILE_E + 1, origTile, 0, Vector2i(TILE_E,1))
		if(tileMap.get_cell_atlas_coords(0, origTile + Vector2i(-1 + adj, 1)) == WALL_TILE_ID):
			tileMap.set_cell(TILE_SW + 1, origTile, 0, Vector2i(TILE_SW,1))
		if(tileMap.get_cell_atlas_coords(0, origTile + Vector2i(0 + adj, 1)) == WALL_TILE_ID):
			tileMap.set_cell(TILE_SE + 1, origTile, 0, Vector2i(TILE_SE,1))
	ball1.score = 0
	ball2.score = 0
	ball3.score = 0
	cherrySprite.rotation = 0
	#world_rotation = PI/2
	#world_gravDir = 1
	updateText()
	generate_map()

func respawnCherry():
	emptyTiles.shuffle()
	cherry.translate(cherry.to_local(tileMap.to_global(tileMap.map_to_local(emptyTiles[0]))))
	newGravity = PhysicsServer2D.area_get_param(
		get_world_2d().space,
		PhysicsServer2D.AREA_PARAM_GRAVITY_VECTOR)
	cherrySprite.rotation = (atan2(newGravity[1], newGravity[0]) - (PI/2 * world_gravDir))
	pass

func prepare_map():
	originalEmptyTiles.clear()
	tileGroup.clear()
	for ty in range(tileMinY, tileMaxY):
		for tx in range(tileMinX, tileMaxX):
			if(tileMap.get_cell_atlas_coords(0, Vector2i(tx, ty)) == FLOOR_TILE_ID):
				originalEmptyTiles.append(Vector2i(tx, ty))
				tileGroup[Vector2i(tx, ty)] = -1
			elif (tileMap.get_cell_atlas_coords(0, Vector2i(tx, ty)) == WALL_TILE_ID):
				tileGroup[Vector2i(tx, ty)] = -1	

func generate_map():
	# Depth-first algorithm, with maximum path lengths
	var visited: Dictionary
	var toVisit: Array
	var pathElements: Dictionary
	var toVisitCount = 0
	emptyTiles.clear()
	for ty in range(tileMinY, tileMaxY):
		var adj = abs(ty) % 2
		for tx in range(tileMinX, tileMaxX):
			var tilePos = Vector2i(tx, ty)
			if(tileMap.get_cell_atlas_coords(0, tilePos) == FLOOR_TILE_ID):
				# start out by putting walls everywhere
				tileMap.set_cell(TILE_NW + 1, tilePos, 0, Vector2i(TILE_NW,1))
				tileMap.set_cell(TILE_NE + 1, tilePos, 0, Vector2i(TILE_NE,1))
				tileMap.set_cell(TILE_W + 1, tilePos, 0, Vector2i(TILE_W,1))
				tileMap.set_cell(TILE_E + 1, tilePos, 0, Vector2i(TILE_E,1))
				tileMap.set_cell(TILE_SW + 1, tilePos, 0, Vector2i(TILE_SW,1))
				tileMap.set_cell(TILE_SE + 1, tilePos, 0, Vector2i(TILE_SE,1))
				emptyTiles.append(tilePos)
				visited[tilePos] = false
				toVisitCount += 1
				toVisit.append(tilePos)
			if(tileMap.get_cell_atlas_coords(0, tilePos) == WALL_TILE_ID):
				visited[tilePos] = true
	var chainRemaining = MAX_PATH_LENGTH
	pathElements[toVisit[randi_range(0, toVisit.size() - 1)]] = true
	toVisitCount -= 1
	var nextPos = pathElements.keys()[randi_range(0, pathElements.size() - 1)]
	while(toVisitCount > 0):
		visited[nextPos] = true
		var adj = abs(nextPos[1]) % 2
		var nextDirs: Array
		var nextOpps: Array
		var nextVisits: Array
		if(!visited[nextPos + Vector2i(adj + -1, -1)]):
			nextVisits.append(nextPos + Vector2i(adj + -1, -1))
			nextDirs.append(TILE_NW)
			nextOpps.append(TILE_SE)
		if(!visited[nextPos + Vector2i(adj + 0, -1)]):
			nextVisits.append(nextPos + Vector2i(adj + 0, -1))
			nextDirs.append(TILE_NE)
			nextOpps.append(TILE_SW)
		if(!visited[nextPos + Vector2i(-1, 0)]):
			nextVisits.append(nextPos + Vector2i(-1, 0))
			nextDirs.append(TILE_W)
			nextOpps.append(TILE_E)
		if(!visited[nextPos + Vector2i(1, 0)]):
			nextVisits.append(nextPos + Vector2i(1, 0))
			nextDirs.append(TILE_E)
			nextOpps.append(TILE_W)
		if(!visited[nextPos + Vector2i(adj + -1, 1)]):
			nextVisits.append(nextPos + Vector2i(adj + -1, 1))
			nextDirs.append(TILE_SW)
			nextOpps.append(TILE_NE)
		if(!visited[nextPos + Vector2i(adj + 0, 1)]):
			nextVisits.append(nextPos + Vector2i(adj + 0, 1))
			nextDirs.append(TILE_SE)
			nextOpps.append(TILE_NW)
		if(nextVisits.size() > 0):
			var visitChoice = randi_range(0, nextVisits.size() - 1)
			var nextVisit = nextVisits[visitChoice]
			var nextDir = nextDirs[visitChoice]
			var nextOpp = nextOpps[visitChoice]
			## Remove separating walls 
			tileMap.erase_cell(nextDir+1, nextPos);
			tileMap.erase_cell(nextOpp+1, nextVisit);
			pathElements[nextVisit] = true
			visited[nextVisit] = true
			toVisitCount -= 1
			if(chainRemaining > 0):
				chainRemaining -= 1
				nextPos = nextVisit
			else: # Extended too far
				chainRemaining = MAX_PATH_LENGTH
				nextPos = pathElements.keys()[randi_range(0, pathElements.size() - 1)]
		else: # No unvisited neighbours
			pathElements.erase(nextPos)
			chainRemaining = MAX_PATH_LENGTH
			nextPos = pathElements.keys()[randi_range(0, pathElements.size() - 1)]
	ball1.setTele()
	ball2.setTele()
	ball3.setTele()
	cherry.setTele()

# Called when the node enters the scene tree for the first time.
func _ready():
	prepare_map()
	generate_map()
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_pressed("player_1_cw"):
		voteStrength1 = voteEnergy1 / 100.0
		world_rotation -= voteStrength1 * voteAdjust * delta * 2 * PI / 360.0
		voteEnergy1 = max(minEnergy, voteEnergy1 - voteSubSpeed * delta)
	elif Input.is_action_pressed("player_1_ccw"):
		voteStrength1 = voteEnergy1 / 100.0
		world_rotation += voteStrength1 * voteAdjust * delta * 2 * PI / 360.0
		voteEnergy1 = max(minEnergy, voteEnergy1 - voteSubSpeed * delta)
	elif Input.is_action_pressed("player_1_gravFlip"):
		if(!teleportActive1):
			teleportActive1 = true
			teleportGravDir1 = world_gravDir
		voteStrength1 = voteEnergy1 / 100.0
		flipProgress += voteStrength1 * voteAdjust * delta
		voteEnergy1 = max(minEnergy, voteEnergy1 - voteSubSpeed * delta)
	else:
		teleportActive1 = false
		voteEnergy1 = min(maxEnergy, voteEnergy1 + voteAddSpeed * delta)
	if Input.is_action_pressed("player_2_cw"):
		voteStrength2 = voteEnergy2 / 100.0
		world_rotation -= voteStrength2 * voteAdjust * delta * 2 * PI / 360.0
		voteEnergy2 = max(minEnergy, voteEnergy2 - voteSubSpeed * delta)
	elif Input.is_action_pressed("player_2_ccw"):
		voteStrength2 = voteEnergy2 / 100.0
		world_rotation += voteStrength2 * voteAdjust * delta * 2 * PI / 360.0
		voteEnergy2 = max(minEnergy, voteEnergy2 - voteSubSpeed * delta)
	elif Input.is_action_pressed("player_2_gravFlip"):
		if(!teleportActive2):
			teleportActive2 = true
			teleportGravDir2 = world_gravDir
		voteStrength2 = voteEnergy2 / 100.0
		flipProgress += voteStrength2 * voteAdjust * delta
		voteEnergy2 = max(minEnergy, voteEnergy2 - voteSubSpeed * delta)
	else:
		teleportActive2 = false
		voteEnergy2 = min(maxEnergy, voteEnergy2 + voteAddSpeed * delta)
	if Input.is_action_pressed("player_3_cw"):
		voteStrength3 = voteEnergy3 / 100.0
		world_rotation -= voteStrength3 * voteAdjust * delta * 2 * PI / 360.0
		voteEnergy3 = max(minEnergy, voteEnergy3 - voteSubSpeed * delta)
	elif Input.is_action_pressed("player_3_ccw"):
		voteStrength3 = voteEnergy3 / 100.0
		world_rotation += voteStrength3 * voteAdjust * delta * 2 * PI / 360.0
		voteEnergy3 = max(minEnergy, voteEnergy3 - voteSubSpeed * delta)
	elif Input.is_action_pressed("player_3_gravFlip"):
		if(!teleportActive3):
			teleportActive3 = true
			teleportGravDir3 = world_gravDir
		voteStrength3 = voteEnergy3 / 100.0
		flipProgress += voteStrength3 * voteAdjust * delta
		voteEnergy3 = max(minEnergy, voteEnergy3 - voteSubSpeed * delta)
	else:
		teleportActive3 = false
		voteEnergy3 = min(maxEnergy, voteEnergy3 + voteAddSpeed * delta)
	if(flipProgress > 100):
		var teleportCount = 0
		if(teleportActive1 && (world_gravDir == -teleportGravDir1)):
			teleportCount += 1
			teleportGravDir1 = 0
			ball1.setTele()
		if(teleportActive2 && (world_gravDir == -teleportGravDir2)):
			teleportCount += 1
			teleportGravDir2 = 0
			ball2.setTele()
		if(teleportActive3 && (world_gravDir == -teleportGravDir3)):
			teleportCount += 1
			teleportGravDir3 = 0
			ball3.setTele()
		if(teleportCount == 0):
			world_gravDir *= -1
		elif(teleportCount == 3):
			respawnGame()
		flipProgress = 0
	flipProgressNode.value = flipProgress
	get_node("player_1_ball/Label").text = "%0.0f" % voteEnergy1;
	get_node("player_2_ball/Label").text = "%0.0f" % voteEnergy2;
	get_node("player_3_ball/Label").text = "%0.0f" % voteEnergy3;
	PhysicsServer2D.area_set_param(get_world_2d().space,
		PhysicsServer2D.AREA_PARAM_GRAVITY_VECTOR,
		world_gravDir * Vector2(cos(world_rotation), sin(world_rotation)))
	newGravity = PhysicsServer2D.area_get_param(
		get_world_2d().space,
		PhysicsServer2D.AREA_PARAM_GRAVITY_VECTOR)
	arrowNode.rotation = atan2(newGravity[1], newGravity[0]) - (PI/2 * world_gravDir)
	arrowDisplayNode.rotation =  (PI/2 * world_gravDir)

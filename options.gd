extends Area2D

signal hit

@onready var rootNode = get_parent()
@onready var tileMap: TileMap = get_node("../menu_TileMap")
@onready var descLabel: Label = get_node("../gravityArrow_Node2D/desc_Label")

var currentOption = ""

# Called when the node enters the scene tree for the first time.
func _ready():
	descLabel.text = ""
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

func select_option():
	if(currentOption == "One Player"):
		get_tree().change_scene_to_file("res://hexMaze_single.tscn")
	if(currentOption == "Three Players"):
		get_tree().change_scene_to_file("res://hexMaze.tscn")
	pass


func _on_body_exited(body):
	print("%s exited %s" % [body.name, name])
	rootNode.setProgress(0.0)
	descLabel.text = ""
	rootNode.inSelection = false
	for c in get_children():
		c.modulate.a = (100.0 / 255.0)
	pass # Replace with function body.


func _on_body_shape_entered(_body_rid, body, _body_shape_index, local_shape_index):
	var tilePos = tileMap.local_to_map(tileMap.to_local(body.global_position))
	var optionShape:CollisionShape2D = get_child(local_shape_index)
	optionShape.modulate.a = 1
	rootNode.inSelection = true
	var optionLabel = optionShape.get_meta('label', "[unknown setting]")
	print("%s entered %s (%s) at (%s)" % [body.name, optionLabel, local_shape_index, str(tilePos)])
	descLabel.text = optionLabel
	currentOption = optionLabel
	pass # Replace with function body.

func _on_input_event(viewport, event, shape_idx):
	if ((event is InputEventMouseButton) and Input.is_mouse_button_pressed(MOUSE_BUTTON_LEFT)):
		var optionShape:CollisionShape2D = get_child(shape_idx)
		var optionLabel = optionShape.get_meta('label', "[unknown setting]")
		if(optionLabel != currentOption):
			rootNode.setProgress(0)
		var shapeLocation:Vector2 = optionShape.global_position
		var optionAngle = shapeLocation.angle()
		rootNode.setTargetAngle(optionAngle)
		print("Clicked on %d, angle %0.2f, world angle %0.2f" % 
			[shape_idx, optionAngle, rootNode.world_rotation])
	pass # Replace with function body.

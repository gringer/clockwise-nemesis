extends RigidBody2D

var kinetic_energy = 0.0
var score = 0
var energy = 100
var cooldown = 0
var playerID = 2
var shouldTele = false

@onready var rootNode = get_parent()

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func addScore(delta):
	score += delta

func setTele():
	shouldTele = true

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	kinetic_energy = linear_velocity.length()
	cooldown = min(100, cooldown + delta * 100.0)
	$CollisionShape2D/Sprite2D.modulate.a = cooldown / 100.0
	pass

func _integrate_forces(state):
	if(shouldTele):
		shouldTele = false
		var xform = state.get_transform()
		xform.origin = rootNode.getRespawnPoint()
		state.set_transform(xform)
	elif((cooldown == 100) and (get_contact_count() > 0)):
		var bodies = get_colliding_bodies()
		for bodyObj in bodies:
			if((bodyObj.name != name) and (bodyObj.name.contains("_ball"))):
				if(kinetic_energy < bodyObj.kinetic_energy):
					cooldown = 0
					bodyObj.addScore(10)
					rootNode.updateText()
					var xform = state.get_transform()
					xform.origin = rootNode.getRespawnPoint()
					state.set_transform(xform)
	pass

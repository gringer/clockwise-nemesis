extends Node2D

@export var world_rotation = PI/2
@export var world_gravDir = 1

var voteStrength1 = 1.0
var voteEnergy1 = 100.0
var teleportActive1 = false
var teleportGravDir1 = 1
var voteStrengthE = 1
var voteEnergyE = 100.0
var voteAdjustE = 50
var teleportActiveE = false
var teleportGravDirE = 1
var voteAddSpeed = 50.0
var voteSubSpeed = 20.0
var flipProgress = 0.0
var voteAdjust = 75
var maxEnergy = 120
var minEnergy = 10

@onready var screenCamera = $gravityArrow_Node2D/gameCamera_Camera2D
@onready var arrowNode =  $gravityArrow_Node2D
@onready var arrowDisplayNode = $gravityArrow_Node2D/gravityDir_Sprite2D
@onready var flipProgressNode = $gravityArrow_Node2D/ProgressBar
@onready var tileMap: TileMap = $walls_TileMap
@onready var ball1 = $player_1_ball
@onready var ballE = $enemy_ball
@onready var cherry = $cherry_Area2D
@onready var cherrySprite = $cherry_Area2D/CollisionPolygon2D/Sprite2D
@onready var scoreText = $gravityArrow_Node2D/scores_RichTextLabel

var newGravity: Vector2

@export var tileLimit = 10
var tileMaxX = tileLimit
var tileMinX = -tileLimit
var tileMaxY = tileLimit
var tileMinY = -tileLimit

# Tilemap constants
const FLOOR_TILE_ID = Vector2i(0, 0)
const WALL_TILE_ID  = Vector2i(1, 0)
const TILEMAP_MISSING = Vector2i(-1,-1)
const TILE_W = 0
const TILE_NW = 1
const TILE_NE = 2
const TILE_E = 3
const TILE_SE = 4
const TILE_SW = 5
const WALL_PUNCH_PROB = 0.5
const MAX_PATH_LENGTH = 12
const DIST_DELTAS = [Vector2i(-1,0), Vector2i(-1,-1),Vector2i( 0,-1),
					 Vector2i( 1,0), Vector2i( 0, 1),Vector2i(-1, 1)]

var originalEmptyTiles: Array
var emptyTiles: Array
var tileGroup: Dictionary

func updateText():
	scoreText.text = str("[font_size=60][b]Scores[/b][/font_size]\n\n",
	"[b]Orange:[/b] ",ball1.score - ballE.score)
	pass

func getRespawnPoint():
	emptyTiles.shuffle()
	return(tileMap.to_global(tileMap.map_to_local(emptyTiles[0])))

func recreateBaseLayer():
	var tileWidth:float = tileMap.map_to_local(Vector2i(0, 0)).distance_to(tileMap.map_to_local(Vector2i(1, 0)))
	var tileHeight:float = (tileWidth + tileWidth / cos(PI / 6)) / 2
	for tileLayer in range(0, 8):
		tileMap.clear_layer(tileLayer)
	var sep:float = 6
	if(sep > 4):
		sep += 0.5
	screenCamera.zoom = Vector2(6 / sep, 6 / sep)
	for ty in range(tileMinY-2, tileMaxY+2):
		for tx in range(tileMinX-2, tileMaxX+2):
			var gridCentreDist = tileMap.map_to_local(Vector2i(0, 0)).\
					distance_to(tileMap.map_to_local(Vector2i(tx, ty))) / tileWidth
			if((gridCentreDist <= (sep + 1)) && (gridCentreDist > (sep))):
				print("putting wall at %s" % Vector2i(tx, ty))
				tileMap.set_cell(0, Vector2i(tx, ty), 0, WALL_TILE_ID)
			elif (gridCentreDist < (sep+1)):
				tileMap.set_cell(0, Vector2i(tx, ty), 0, FLOOR_TILE_ID)

func respawnGame():
	for layerID in range(1, 6):
		tileMap.clear_layer(layerID)
	for origTile in originalEmptyTiles:
		tileMap.set_cell(0, origTile, 0, FLOOR_TILE_ID)
		# Add back walls to stop out of bounds falling
		var adj = abs(origTile[1]) % 2
		if(tileMap.get_cell_atlas_coords(0, origTile + Vector2i(-1, 0)) == WALL_TILE_ID):
			tileMap.set_cell(TILE_W + 1, origTile, 0, Vector2i(TILE_W,1))
		if(tileMap.get_cell_atlas_coords(0, origTile + Vector2i(-1 + adj, -1)) == WALL_TILE_ID):
			tileMap.set_cell(TILE_NW + 1, origTile, 0, Vector2i(TILE_NW,1))
		if(tileMap.get_cell_atlas_coords(0, origTile + Vector2i(0 + adj, -1)) == WALL_TILE_ID):
			tileMap.set_cell(TILE_NE + 1, origTile, 0, Vector2i(TILE_NE,1))
		if(tileMap.get_cell_atlas_coords(0, origTile + Vector2i(1, 0)) == WALL_TILE_ID):
			tileMap.set_cell(TILE_E + 1, origTile, 0, Vector2i(TILE_E,1))
		if(tileMap.get_cell_atlas_coords(0, origTile + Vector2i(0 + adj, 1)) == WALL_TILE_ID):
			tileMap.set_cell(TILE_SE + 1, origTile, 0, Vector2i(TILE_SE,1))
		if(tileMap.get_cell_atlas_coords(0, origTile + Vector2i(-1 + adj, 1)) == WALL_TILE_ID):
			tileMap.set_cell(TILE_SW + 1, origTile, 0, Vector2i(TILE_SW,1))
	ball1.score = 0
	ballE.score = 0
	cherrySprite.rotation = 0
	updateText()
	generate_map()

func respawnCherry():
	emptyTiles.shuffle()
	cherry.translate(cherry.to_local(tileMap.to_global(tileMap.map_to_local(emptyTiles[0]))))
	newGravity = PhysicsServer2D.area_get_param(
		get_world_2d().space,
		PhysicsServer2D.AREA_PARAM_GRAVITY_VECTOR)
	cherrySprite.rotation = (atan2(newGravity[1], newGravity[0]) - (PI/2 * world_gravDir))
	pass

func prepare_map():
	originalEmptyTiles.clear()
	tileGroup.clear()
	for ty in range(tileMinY, tileMaxY):
		for tx in range(tileMinX, tileMaxX):
			if(tileMap.get_cell_atlas_coords(0, Vector2i(tx, ty)) == FLOOR_TILE_ID):
				originalEmptyTiles.append(Vector2i(tx, ty))
				tileGroup[Vector2i(tx, ty)] = -1
			elif (tileMap.get_cell_atlas_coords(0, Vector2i(tx, ty)) == WALL_TILE_ID):
				tileGroup[Vector2i(tx, ty)] = -1	

func generate_map():
	# Depth-first algorithm, with maximum path lengths
	var visited: Dictionary
	var toVisit: Array
	var pathElements: Dictionary
	var toVisitCount = 0
	emptyTiles.clear()
	for ty in range(tileMinY, tileMaxY):
		var adj = abs(ty) % 2
		for tx in range(tileMinX, tileMaxX):
			var tilePos = Vector2i(tx, ty)
			if(tileMap.get_cell_atlas_coords(0, tilePos) == FLOOR_TILE_ID):
				# start out by putting walls everywhere
				tileMap.set_cell(TILE_NW + 1, tilePos, 0, Vector2i(TILE_NW,1))
				tileMap.set_cell(TILE_NE + 1, tilePos, 0, Vector2i(TILE_NE,1))
				tileMap.set_cell(TILE_W + 1, tilePos, 0, Vector2i(TILE_W,1))
				tileMap.set_cell(TILE_E + 1, tilePos, 0, Vector2i(TILE_E,1))
				tileMap.set_cell(TILE_SW + 1, tilePos, 0, Vector2i(TILE_SW,1))
				tileMap.set_cell(TILE_SE + 1, tilePos, 0, Vector2i(TILE_SE,1))
				emptyTiles.append(tilePos)
				visited[tilePos] = false
				toVisitCount += 1
				toVisit.append(tilePos)
			if(tileMap.get_cell_atlas_coords(0, tilePos) == WALL_TILE_ID):
				visited[tilePos] = true
	var chainRemaining = MAX_PATH_LENGTH
	pathElements[toVisit[randi_range(0, toVisit.size() - 1)]] = true
	toVisitCount -= 1
	var nextPos = pathElements.keys()[randi_range(0, pathElements.size() - 1)]
	while(toVisitCount > 0):
		visited[nextPos] = true
		var adj = abs(nextPos[1]) % 2
		var nextDirs: Array
		var nextOpps: Array
		var nextVisits: Array
		if(!visited[nextPos + Vector2i(adj + -1, -1)]):
			nextVisits.append(nextPos + Vector2i(adj + -1, -1))
			nextDirs.append(TILE_NW)
			nextOpps.append(TILE_SE)
		if(!visited[nextPos + Vector2i(adj + 0, -1)]):
			nextVisits.append(nextPos + Vector2i(adj + 0, -1))
			nextDirs.append(TILE_NE)
			nextOpps.append(TILE_SW)
		if(!visited[nextPos + Vector2i(-1, 0)]):
			nextVisits.append(nextPos + Vector2i(-1, 0))
			nextDirs.append(TILE_W)
			nextOpps.append(TILE_E)
		if(!visited[nextPos + Vector2i(1, 0)]):
			nextVisits.append(nextPos + Vector2i(1, 0))
			nextDirs.append(TILE_E)
			nextOpps.append(TILE_W)
		if(!visited[nextPos + Vector2i(adj + -1, 1)]):
			nextVisits.append(nextPos + Vector2i(adj + -1, 1))
			nextDirs.append(TILE_SW)
			nextOpps.append(TILE_NE)
		if(!visited[nextPos + Vector2i(adj + 0, 1)]):
			nextVisits.append(nextPos + Vector2i(adj + 0, 1))
			nextDirs.append(TILE_SE)
			nextOpps.append(TILE_NW)
		if(nextVisits.size() > 0):
			var visitChoice = randi_range(0, nextVisits.size() - 1)
			var nextVisit = nextVisits[visitChoice]
			var nextDir = nextDirs[visitChoice]
			var nextOpp = nextOpps[visitChoice]
			## Remove separating walls 
			tileMap.erase_cell(nextDir+1, nextPos);
			tileMap.erase_cell(nextOpp+1, nextVisit);
			pathElements[nextVisit] = true
			visited[nextVisit] = true
			toVisitCount -= 1
			if(chainRemaining > 0):
				chainRemaining -= 1
				nextPos = nextVisit
			else: # Extended too far
				chainRemaining = MAX_PATH_LENGTH
				nextPos = pathElements.keys()[randi_range(0, pathElements.size() - 1)]
		else: # No unvisited neighbours
			pathElements.erase(nextPos)
			chainRemaining = MAX_PATH_LENGTH
			nextPos = pathElements.keys()[randi_range(0, pathElements.size() - 1)]
	ball1.setTele()
	ballE.setTele()
	cherry.setTele()

# Called when the node enters the scene tree for the first time.
func _ready():
	recreateBaseLayer()
	prepare_map()
	generate_map()
	updateText()
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if(ballE.tryingToMove):
		var minDelta = voteStrengthE * voteAdjustE * delta * 2 * PI / 360.0
		var shiftAngle = ballE.targetAngle - world_rotation
		var shiftDirectly = false
		while(abs(shiftAngle) > PI):
			shiftAngle += -sign(shiftAngle) * 2 * PI
		if(abs(shiftAngle) > minDelta):
			shiftAngle = ballE.targetAngle - world_rotation
			while(abs(shiftAngle) > PI):
				shiftAngle += -sign(shiftAngle) * 2 * PI
		else:
			if(shiftAngle != 0):
				shiftDirectly = true
			shiftAngle = 0
		if(shiftDirectly):
			voteStrengthE = voteEnergyE / 100.0
			world_rotation = ballE.targetAngle
			voteEnergyE = max(minEnergy, voteEnergyE - voteSubSpeed * delta)
		if((shiftAngle < -PI * 0.85) or (shiftAngle > PI * 0.85)):
			voteStrengthE = voteEnergyE / 100.0
			flipProgress += voteStrengthE * voteAdjust * delta
			voteEnergyE = max(minEnergy, voteEnergyE - voteSubSpeed * delta)
		elif(shiftAngle < 0):
			voteStrengthE = voteEnergyE / 100.0
			world_rotation -= voteStrengthE * voteAdjustE * delta * 2 * PI / 360.0
			voteEnergyE = max(minEnergy, voteEnergyE - voteSubSpeed * delta)
		elif(shiftAngle > 0):
			voteStrengthE = voteEnergyE / 100.0
			world_rotation += voteStrengthE * voteAdjustE * delta * 2 * PI / 360.0
			voteEnergyE = max(minEnergy, voteEnergyE - voteSubSpeed * delta)
		else:
			ballE.targetAngle = world_rotation
			voteEnergyE = min(maxEnergy, voteEnergy1 + voteAddSpeed * delta)
	else:
		ballE.targetAngle = world_rotation
		voteEnergyE = min(maxEnergy, voteEnergy1 + voteAddSpeed * delta)
	if(world_gravDir == -1):
		ballE.get_node("Sprite2D").rotation = - ballE.rotation + ballE.targetAngle + PI
	else:	
		ballE.get_node("Sprite2D").rotation = - ballE.rotation + ballE.targetAngle
	if Input.is_action_pressed("player_1_cw"):
		voteStrength1 = voteEnergy1 / 100.0
		world_rotation -= voteStrength1 * voteAdjust * delta * 2 * PI / 360.0
		voteEnergy1 = max(minEnergy, voteEnergy1 - voteSubSpeed * delta)
	elif Input.is_action_pressed("player_1_ccw"):
		voteStrength1 = voteEnergy1 / 100.0
		world_rotation += voteStrength1 * voteAdjust * delta * 2 * PI / 360.0
		voteEnergy1 = max(minEnergy, voteEnergy1 - voteSubSpeed * delta)
	elif Input.is_action_pressed("player_1_gravFlip"):
		if(!teleportActive1):
			teleportActive1 = true
			teleportGravDir1 = world_gravDir
		voteStrength1 = voteEnergy1 / 100.0
		flipProgress += voteStrength1 * voteAdjust * delta
		voteEnergy1 = max(minEnergy, voteEnergy1 - voteSubSpeed * delta)
	else:
		teleportActive1 = false
		voteEnergy1 = min(maxEnergy, voteEnergy1 + voteAddSpeed * delta)
	if(flipProgress > 100):
		var teleportCount = 0
		if(teleportActive1 && (world_gravDir == -teleportGravDir1)):
			teleportCount += 1
			teleportGravDir1 = 0
			ball1.setTele()
		if(teleportCount == 0):
			world_gravDir *= -1
		elif(teleportCount == 1):
			respawnGame()
		flipProgress = 0
	flipProgressNode.value = flipProgress
	get_node("player_1_ball/Label").text = "%0.0f" % voteEnergy1;
	get_node("enemy_ball/Label").text = "%0.0f" % voteEnergyE;
	PhysicsServer2D.area_set_param(get_world_2d().space,
		PhysicsServer2D.AREA_PARAM_GRAVITY_VECTOR,
		world_gravDir * Vector2(cos(world_rotation), sin(world_rotation)))
	newGravity = PhysicsServer2D.area_get_param(
		get_world_2d().space,
		PhysicsServer2D.AREA_PARAM_GRAVITY_VECTOR)
	arrowNode.rotation = atan2(newGravity[1], newGravity[0]) - (PI/2 * world_gravDir)
	arrowDisplayNode.rotation =  (PI/2 * world_gravDir)

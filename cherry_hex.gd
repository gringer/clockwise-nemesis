extends Area2D

signal hit

@onready var rootNode = get_parent()
var lastPlayerTouch = -1
var lastPlayerScore = 1
var shouldTele = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func setTele():
	var newLocation = rootNode.getRespawnPoint()
	position = newLocation
	rotation = rootNode.world_rotation - PI/2

func _on_body_entered(bodyObj):
	if(bodyObj.name != "walls_TileMap"):
		if(bodyObj.playerID == lastPlayerTouch):
			bodyObj.addScore(lastPlayerScore)
			lastPlayerScore = min(32, lastPlayerScore * 2)
		else:
			bodyObj.addScore(1)
			lastPlayerTouch = bodyObj.playerID
			lastPlayerScore = 2
		rootNode.updateText()
		setTele()
